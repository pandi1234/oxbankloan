package com.hcl.oxbank.service;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.hcl.oxbank.Utils.ApplicationConstants;
import com.hcl.oxbank.entity.Customer;
import com.hcl.oxbank.entity.Loan;
import com.hcl.oxbank.exception.LoanDetailsNotFoundException;
import com.hcl.oxbank.repository.LoanRepository;
import java.util.ArrayList;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


class LoanAcceptanceServiceTest
{
	@Mock
	private CustomerService customerService;

	@Mock
	private LoanRepository loanRepository;

	@InjectMocks
	private LoanAcceptanceService loanAcceptanceService;

	@Test
	void testGetLoanDetailsByCustomerId() 
	{
		MockitoAnnotations.openMocks(this);
		int customerId = 123;
		Customer customer = new Customer();
		List<Loan> loans = new ArrayList<>();
		loans.add(new Loan());
		customer.setLoans(loans);
		when(customerService.getDataByID(customerId)).thenReturn(customer);
		List<Loan> result = loanAcceptanceService.getLoanDetailsByCustomerId(customerId);
		assertEquals(loans, result);
	}
	
	@Test
	public void testGetLoanDetailsByCustomerId_LoanNotFound()
	{
		MockitoAnnotations.openMocks(this);
		int customerId = 123;
		Customer customer = new Customer();
		when(customerService.getDataByID(customerId)).thenReturn(customer);
		assertThrows(LoanDetailsNotFoundException.class,
		() -> loanAcceptanceService.getLoanDetailsByCustomerId(customerId));
	}

	@Test
	void testUpdateLoanStatus() 
	{
		MockitoAnnotations.openMocks(this);
		int customerId = 123;
		Customer customer = new Customer();
		List<Loan> loans = new ArrayList<>();
		loans.add(new Loan());
		customer.setLoans(loans);
		when(customerService.getDataByID(customerId)).thenReturn(customer);
		loanAcceptanceService.updateLoanStatus(customerId);
		for (Loan loan : loans)
		{
			assertEquals(ApplicationConstants.loanstatusprocessing, loan.getLoanStatus());
		}
		verify(loanRepository).saveAll(loans);
	}
}
