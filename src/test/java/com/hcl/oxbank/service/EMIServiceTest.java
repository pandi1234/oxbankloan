package com.hcl.oxbank.service;

import org.junit.jupiter.api.Assertions; 
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class EMIServiceTest {

	private EMIService emiService;

	@BeforeEach
	public void setup() {
		emiService = new EMIService();
	}

	@Test
	public void testCalculateEMIAmountWithValidValues() {

		double loanAmount = 10000;
		int tenure = 12;

		double emiAmount = emiService.calculateEMIAmount(loanAmount, tenure);

		double expectedEMIAmount = 888.4878867834168;
		double delta = 0.01;
		Assertions.assertEquals(expectedEMIAmount, emiAmount, delta);
	}
}