package com.hcl.oxbank.service;

import org.junit.jupiter.api.Test; 
import org.junit.jupiter.api.Assertions;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import com.hcl.oxbank.dto.LoanEligibilityDto;
import com.hcl.oxbank.dto.LoanEligibilityResponse;
import com.hcl.oxbank.entity.Customer;
import com.hcl.oxbank.entity.Loan;
import com.hcl.oxbank.exception.EligibilityValidationException;
import com.hcl.oxbank.repository.CustomerRepository;
import com.hcl.oxbank.repository.LoanRepository;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class LoanEligibilityServiceTest {
	@Mock
	private EligibilityService eligibilityService;

	@Mock
	private EMIService emiService;

	@Mock
	private CustomerRepository customerRepository;

	@Mock
	private LoanRepository loanRepository;

	@InjectMocks
	private LoanEligibilityService loanEligibilityService;

	@Test
	public void testCheckLoanEligibility_Eligible() throws ParseException, EligibilityValidationException {
		MockitoAnnotations.openMocks(this);

		LoanEligibilityDto loanEligibilityDto = new LoanEligibilityDto();
		loanEligibilityDto.setWorkExperience(25);
		loanEligibilityDto.setId(9);

		Customer customer = new Customer();
		customer.setLoans(new ArrayList<>());
		when(customerRepository.findById(9)).thenReturn(Optional.of(customer));

		List<Loan> loanList =loanEligibilityService.getLoanDetailsByCustomerId(9);
		for(Loan loan : loanList) {
			loan.setLoanAmount(10000);
			loan.setTenure(12);
			List<Loan> loans = new ArrayList<>();
			loans.add(loan);
			when(loanRepository.saveAll(loanList)).thenReturn(loanList);
			when(emiService.calculateEMIAmount(10000, 12)).thenReturn(888.4878867834168);
			LoanEligibilityResponse response = loanEligibilityService.checkLoanEligibility(loanEligibilityDto);

			assertEquals(888.4878867834168, response.getEmi(), 0.0);
			assertEquals("Enquiry", loan.getLoanStatus());
			verify(loanRepository, times(1)).saveAll(loanList);
		}
	}

	@Test
	public void testCheckLoanEligibilityNotEligible() throws ParseException{
		MockitoAnnotations.openMocks(this);
		LoanEligibilityDto loanEligibilityDto = new LoanEligibilityDto();
		loanEligibilityDto.setWorkExperience(23);

		List<Loan> loans = new ArrayList<>();
		Customer customer = new Customer(5, "ABC", "234785908", "18/10/2001", "Male", "Single", 232, 7000, 4000, loans);

		when(customerRepository.findById(5)).thenReturn(Optional.of(customer));
		List<Loan> loanList =loanEligibilityService.getLoanDetailsByCustomerId(5);
		for(Loan loan:loanList) {
			loan.setLoanStatus("Rejected");
			assertEquals("Rejected",loan.getLoanStatus());
			Assertions.assertThrows(EligibilityValidationException.class,
					() -> loanEligibilityService.checkLoanEligibility(loanEligibilityDto));
		}
	}
}
