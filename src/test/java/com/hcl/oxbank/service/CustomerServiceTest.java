package com.hcl.oxbank.service;

import com.hcl.oxbank.dto.CustomerDto;
import com.hcl.oxbank.entity.Customer;
import com.hcl.oxbank.exception.CustomerNotFoundException;
import com.hcl.oxbank.repository.CustomerRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.Optional;

@ExtendWith({MockitoExtension.class})
public class CustomerServiceTest {

    @Mock
    private CustomerRepository customerRepository;

    @InjectMocks
    private CustomerService customerService;

    public CustomerServiceTest() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testDatasave() {
        Customer customer = createcustomer(createcustomerDto());
        Mockito.when(customerRepository.save(customer)).thenReturn(customer);
        Customer result = customerService.datasave(createcustomerDto());
        Assertions.assertEquals(customer.getName(), result.getName());
        Assertions.assertEquals(customer.getMobile(), result.getMobile());
        Assertions.assertEquals(customer.getDob(), result.getDob());
        Assertions.assertEquals(customer.getGender(), result.getGender());
        Assertions.assertEquals(customer.getMaritalstatus(), result.getMaritalstatus());
        Assertions.assertEquals(customer.getCreditscore(), result.getCreditscore());
        Assertions.assertEquals(customer.getSalary(), result.getSalary());
        Assertions.assertEquals(customer.getExpense(), result.getExpense());
    }

    private CustomerDto createcustomerDto() {
        CustomerDto customerDto = new CustomerDto();
        customerDto.setName("abcde");
        customerDto.setMobile("8073843043");
        customerDto.setDob("01/02/1998");
        customerDto.setGender("female");
        customerDto.setMaritalstatus("single");
        customerDto.setCreditscore(300);
        customerDto.setSalary(40000);
        customerDto.setExpense(4000);
        return customerDto;
    }

    private Customer createcustomer(CustomerDto customerDto) {
        Customer customer = new Customer();
        customer.setName(customerDto.getName());
        customer.setMobile(customerDto.getMobile());
        customer.setDob(customerDto.getDob());
        customer.setGender(customerDto.getGender());
        customer.setMaritalstatus(customerDto.getMaritalstatus());
        customer.setCreditscore(customerDto.getCreditscore());
        customer.setSalary(customerDto.getSalary());
        customer.setExpense(customerDto.getExpense());
        return customer;
    }

    @Test
    public void testGetDataByID() throws CustomerNotFoundException {
        int customerID = 1;
        Customer customer = new Customer();
        Mockito.when(customerRepository.findById(customerID)).thenReturn(Optional.of(customer));
        Customer result = customerService.getDataByID(customerID);
        Mockito.verify(customerRepository, Mockito.times(1)).findById(customerID);
        Assertions.assertEquals(customer, result);
    }

    @Test
    public void testGetDataByID_CustomerNotFound() {
        int customerID = 1;
        Mockito.when(customerRepository.findById(customerID)).thenReturn(Optional.empty());
        Assertions.assertThrows(CustomerNotFoundException.class, () -> customerService.getDataByID(customerID));
        Mockito.verify(customerRepository, Mockito.times(1)).findById(customerID);
    }
}