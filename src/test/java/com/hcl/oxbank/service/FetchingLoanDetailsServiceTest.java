package com.hcl.oxbank.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import com.hcl.oxbank.dto.LoanDto;
import com.hcl.oxbank.entity.Customer;
import com.hcl.oxbank.entity.Loan;
import com.hcl.oxbank.exception.CustomerNotFoundException;
import com.hcl.oxbank.repository.CustomerRepository;
import com.hcl.oxbank.repository.LoanRepository;
import com.hcl.oxbank.service.CustomerService;
import com.hcl.oxbank.service.EMIService;
import com.hcl.oxbank.service.LoanService;

public class FetchingLoanDetailsServiceTest {

	@Mock
	private CustomerRepository customerRepository;
	@Mock
	CustomerService customerService;
	@Mock
	private EMIService emiService;
	@Mock
	private LoanRepository loanRepository;
	@InjectMocks
	private LoanService loanService;
	
	@Test
	public void testGetLoanDetailsByCustomerId() {
		MockitoAnnotations.initMocks(this);
		int customerId = 42;
		Customer customer = new Customer();
		customer.setId(customerId);
		Loan loan1 = new Loan();
		loan1.setLoanAccountNumber(123);
		loan1.setLoanAmount(1000);
		loan1.setTenure(12);
		loan1.setCustomers(customer);
		Loan loan2 = new Loan();
		loan2.setLoanAccountNumber(234);
		loan2.setLoanAmount(2000);
		loan2.setTenure(24);
		loan2.setCustomers(customer);
		List<Loan> loans = new ArrayList<>();
		loans.add(loan1);
		loans.add(loan2);
		customer.setLoans(loans);
		when(customerRepository.findById(customerId)).thenReturn(Optional.of(customer));	
		when(emiService.calculateEMIAmount(loan1.getLoanAmount(), loan1.getTenure())).thenReturn(83.33);
		when(emiService.calculateEMIAmount(loan2.getLoanAmount(), loan2.getTenure())).thenReturn(166.67);
		List<LoanDto> loanDtoList = loanService.fetchLoanDetails(customerId);		
		assertEquals(2, loanDtoList.size());
		LoanDto loanDto1 = loanDtoList.get(0);
		assertEquals(123, loanDto1.getLoanAccountNumber());
		assertEquals(1000, loanDto1.getLoanAmount(), 0.001);
		assertEquals(12, loanDto1.getTenure());
		assertEquals(loanDto1.getInterest(), 0.12);
		assertEquals(83.33,loanDto1.getEMI(),0.001);
		LoanDto loanDto2 = loanDtoList.get(1);
		assertEquals(234, loanDto2.getLoanAccountNumber());
		assertEquals(2000, loanDto2.getLoanAmount(), 0.001);
		assertEquals(24, loanDto2.getTenure());
		assertEquals(loanDto2.getInterest(), 0.12);
		assertEquals(166.67,loanDto2.getEMI(),0.001);
		verify(customerRepository, times(1)).findById(customerId);
		verify(emiService, times(1)).calculateEMIAmount(loan1.getLoanAmount(), loan1.getTenure());
		verify(emiService, times(1)).calculateEMIAmount(loan2.getLoanAmount(), loan2.getTenure());
		
	}
	
	@Test
    public void testGetLoanDetailsByCustomerId_CustomerNotFound()
    {
        MockitoAnnotations.openMocks(this);
        int customerId = 123;
        Customer customer = new Customer();
        when(customerService.getDataByID(customerId)).thenReturn(customer);
        assertThrows(CustomerNotFoundException.class,
        () ->loanService.fetchLoanDetails(customerId));
    }
}
