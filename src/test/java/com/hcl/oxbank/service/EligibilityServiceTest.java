package com.hcl.oxbank.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import com.hcl.oxbank.entity.Customer;
import com.hcl.oxbank.entity.Loan;
import com.hcl.oxbank.exception.EligibilityValidationException;
import com.hcl.oxbank.repository.CustomerRepository;
import com.hcl.oxbank.service.EligibilityService;

public class EligibilityServiceTest {

	@Mock
	private CustomerRepository customerRepository;
	@InjectMocks
	private EligibilityService eligibilityService;

	@BeforeEach
	public void setup() {
	MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testInvalidExperience() {
	List<Loan> loans = new ArrayList<>();
	Customer customer = new Customer(1, "ABC", "234785908", "18/10/2001", "Male", "Single", 232, 7000, 4000, loans);
	Mockito.when(customerRepository.getById(1)).thenReturn(customer);
	Assertions.assertThrows(EligibilityValidationException.class,
	() -> eligibilityService.checkEligibility(23, 1));
	}

	@Test
	public void testInvalidDateFormat() {
	List<Loan> loans = new ArrayList<>();
	Customer customer = new Customer(1, "ABC", "234785908", "Invalid Date", "Male", "Single", 232, 7000, 4000, loans);
	Mockito.when(customerRepository.getById(1)).thenReturn(customer);
	Assertions.assertThrows(ParseException.class,
	() -> eligibilityService.checkEligibility(24, 1));
	}
	@Test
	public void testInvalidAge() {
	List<Loan> loans = new ArrayList<>();
	Customer customer = new Customer(1, "ABC", "234785908", "18/10/2001", "Male", "Single", 232, 7000, 4000, loans);
	Mockito.when(customerRepository.getById(1)).thenReturn(customer);
	Assertions.assertThrows(EligibilityValidationException.class,
	() -> eligibilityService.checkEligibility(26, 1));
	}

	@Test
	public void testCalculateAge() 
	{
	Calendar dobCalendar = Calendar.getInstance();
	dobCalendar.set(1990, Calendar.JANUARY, 1);
	Date dob = dobCalendar.getTime();
	Calendar currentCalendar = Calendar.getInstance();
	currentCalendar.set(2023, Calendar.JULY, 3);
	Date currentDate = currentCalendar.getTime();
	int age =  eligibilityService.calculateAge(dob, currentDate);
	assertEquals(33, age, "Age criteria is not matching");
	}
	

	
}
