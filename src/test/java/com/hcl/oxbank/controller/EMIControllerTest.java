package com.hcl.oxbank.controller;

import org.junit.jupiter.api.Test;  
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import com.hcl.oxbank.dto.EMIdto;
import com.hcl.oxbank.entity.Customer;
import com.hcl.oxbank.entity.Loan;
import com.hcl.oxbank.service.CustomerService;
import com.hcl.oxbank.service.EMIService;
import com.hcl.oxbank.service.LoanService;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@WebMvcTest(EMIController.class)
public class EMIControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private CustomerService customerService;

	@MockBean
	private EMIService emiService;

	@MockBean
	private LoanService loanService;

	@Test
	public void testCalculateEMI() throws Exception {
		EMIdto emidto = new EMIdto();
		emidto.setId(10);
		emidto.setLoanAmount(100000);
		emidto.setTenure(60);

		Customer customer = new Customer();
		customer.setId(10);
		customer.setName("John Doe");
		when(customerService.getDataByID(10)).thenReturn(customer);

		double emiAmount = 2000.0;
		when(emiService.calculateEMIAmount(100000, 60)).thenReturn(emiAmount);

		Loan savedLoan = new Loan();
		savedLoan.setLoanAccountNumber(1);
		savedLoan.setTenure(5);
		savedLoan.setLoanAmount(100000);
		savedLoan.setCustomers(customer);
		when(loanService.saveLoanDetails(any(Loan.class))).thenReturn(savedLoan);

		mockMvc.perform(MockMvcRequestBuilders.post("/EMI")
				.contentType(MediaType.APPLICATION_JSON)
				.content("{\"id\": 10, \"loanAmount\": 100000, \"tenure\": 60}"))
		.andExpect(MockMvcResultMatchers.status().isOk());
	}
}