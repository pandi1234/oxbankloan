package com.hcl.oxbank.controller;

import com.hcl.oxbank.dto.CustomerDto;
import com.hcl.oxbank.exception.DateException;
import com.hcl.oxbank.service.CustomerService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.mockito.Mockito.verify;

@ExtendWith({MockitoExtension.class})
public class CustomerControllerTest {

    @Mock
    private CustomerService customerService;

    @InjectMocks
    private CustomerController customerController;
    CustomerDto customerDto = new CustomerDto();

    @Test
    public void testCustomerSave() throws DateException {
        String dob = "02/03/1998";
        customerDto.setDob(dob);
        String result = customerController.customerSave(customerDto);
        String expected = "you have registered successfully";
        Assertions.assertEquals(expected, result);
        verify(customerService).datasave(customerDto);
    }

    @Test
    public void TestCustomerFutureDate() throws DateException {
        String dob = "02/07/2025";
        customerDto.setDob(dob);
        CustomerController customerController = new CustomerController();
        Assertions.assertThrows(DateException.class, () -> {
            customerController.customerSave(customerDto);
        });
    }

    @Test
    public void TestCustomerInvalidDate() throws DateException {
        String dob = "32/13/2002";
        customerDto.setDob(dob);
        CustomerController customerController = new CustomerController();
        Assertions.assertThrows(DateException.class, () -> {
            customerController.customerSave(customerDto);
        });
        Mockito.verifyNoInteractions(customerService);
    }
}