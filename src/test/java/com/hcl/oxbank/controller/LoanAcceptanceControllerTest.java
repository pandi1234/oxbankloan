package com.hcl.oxbank.controller;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import com.hcl.oxbank.dto.LoanAcceptanceDto;
import com.hcl.oxbank.dto.LoanAcceptanceResponse;
import com.hcl.oxbank.entity.Loan;
import com.hcl.oxbank.service.EMIService;
import com.hcl.oxbank.service.LoanAcceptanceService;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyDouble;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;

public class LoanAcceptanceControllerTest 
{
	@Mock
	private LoanAcceptanceService loanAcceptanceService;
	@Mock
	private EMIService emiService;
	@InjectMocks
	private LoanAcceptanceController loanAcceptanceController;
	@Test
	public void testAcceptLoan() 
	{
		MockitoAnnotations.initMocks(this);
		LoanAcceptanceDto loanAcceptanceDto = new LoanAcceptanceDto();
		loanAcceptanceDto.setId(123);
		Loan loan = new Loan();
		loan.setLoanAmount(1000);
		loan.setTenure(12);
		when(loanAcceptanceService.getLoanDetailsByCustomerId(anyInt())).thenReturn(List.of(loan));
		when(emiService.calculateEMIAmount(anyDouble(), anyInt())).thenReturn(1200.0);
		List<LoanAcceptanceResponse> response = loanAcceptanceController.acceptLoan(loanAcceptanceDto);
		verify(loanAcceptanceService).updateLoanStatus(loanAcceptanceDto.getId());
		verify(loanAcceptanceService).getLoanDetailsByCustomerId(anyInt());
		verify(emiService).calculateEMIAmount(anyDouble(), anyInt());
		assertEquals(1000.0, response.get(0).getLoanAmount());
		assertEquals(12,response.get(0).getTenure());
		assertEquals(1200.0,response.get(0).getEmiAmount());
	}
}