package com.hcl.oxbank.controller;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import com.hcl.oxbank.dto.EligibilityResponse;
import com.hcl.oxbank.entity.Customer;
import com.hcl.oxbank.entity.Loan;
import com.hcl.oxbank.exception.EligibilityValidationException;
import com.hcl.oxbank.service.EligibilityService;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;


@SpringBootTest
public class EligibilityControllerTest {
	
    @Mock
    private EligibilityService eligibilityService;

    @InjectMocks
    private EligibilityController eligibilityController;
    
    @Test
    public void testCheckEligibility_Eligible() throws EligibilityValidationException, ParseException {
        int workExperience = 5;
        int ID = 123;
        when(eligibilityService.isCustomerEligible(workExperience, ID)).thenReturn(true);
        List<Loan> loans = new ArrayList<>();
    	Customer customer = new Customer(1, "ABC", "234785908", "18/10/2001", "Male", "Single", 232, 7000, 4000, loans);
        EligibilityResponse response = eligibilityController.checkEligibility(workExperience, ID);
        assertEquals("Congratulations! You are eligible for the loan.", response.getMessage());

    }

}
