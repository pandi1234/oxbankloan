package com.hcl.oxbank.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import com.hcl.oxbank.controller.LoanController;
import com.hcl.oxbank.dto.LoanDto;
import com.hcl.oxbank.service.LoanService;

public class FetchingLoanDetailsControllerTest {
	@Mock
	private LoanService loanService;

	@InjectMocks
	private LoanController loanController;

	@BeforeEach
	void setup() {
	MockitoAnnotations.openMocks(this);
	}

	@Test
	public void testGetLoanDetailsByCustomerId() {
	int customerId = 123;
	LoanDto loanDto1 = new LoanDto(123,1,100000,60,0.12,4569000);
	LoanDto loanDto2 = new LoanDto(124,2,200000,60,0.12,4569000);
	List<LoanDto> expectedLoanDetails = Arrays.asList(loanDto1, loanDto2);
	when(loanService.fetchLoanDetails(customerId)).thenReturn(expectedLoanDetails);
	List<LoanDto> result = loanController.getLoanDetailsByCustomerId(customerId);
	verify(loanService, times(1)).fetchLoanDetails(customerId);
	assertEquals(expectedLoanDetails, result);
	}
	}
