package com.hcl.oxbank.controller;

import org.junit.jupiter.api.Test; 
import static org.mockito.Mockito.when;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hcl.oxbank.dto.LoanEligibilityDto;
import com.hcl.oxbank.dto.LoanEligibilityResponse;
import com.hcl.oxbank.service.LoanEligibilityService;
public class LoanEligibilityControllerTest {

	private MockMvc mockMvc;
	@Mock
	private LoanEligibilityService loanEligibilityService;

	@InjectMocks
	private LoanEligibilityController loanEligibilityController;

	@BeforeEach
	public void setup() {
	MockitoAnnotations.openMocks(this);
	mockMvc = MockMvcBuilders.standaloneSetup(loanEligibilityController).build();
	}

	@Test
	public void testCheckLoanEligibilityController() throws Exception {
	LoanEligibilityDto loanEligibilityDto = new LoanEligibilityDto();
	LoanEligibilityResponse expectedResponse = new LoanEligibilityResponse();

	when(loanEligibilityService.checkLoanEligibility(loanEligibilityDto)).thenReturn(expectedResponse);

	mockMvc.perform(MockMvcRequestBuilders.post("/checkLoanEligibility")
	.content(asJsonString(loanEligibilityDto))
	.contentType(MediaType.APPLICATION_JSON))
	.andExpect(MockMvcResultMatchers.status().isOk())
	.andExpect(MockMvcResultMatchers.content().json(asJsonString(expectedResponse)));
	}

	private static String asJsonString(final Object obj) {
	try {
	return new ObjectMapper().writeValueAsString(obj);
	} catch (Exception e) {
	throw new RuntimeException(e);
	}
	}
}
