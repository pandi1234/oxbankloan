package com.hcl.oxbank.controller;
import java.text.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.hcl.oxbank.dto.LoanEligibilityDto;
import com.hcl.oxbank.dto.LoanEligibilityResponse;
import com.hcl.oxbank.service.LoanEligibilityService;

@RestController
public class LoanEligibilityController {
	@Autowired
	LoanEligibilityService loanEligibilityService;
	
	@PostMapping("/checkLoanEligibility")
	public LoanEligibilityResponse checkLoanEligibility(@RequestBody LoanEligibilityDto loanEligibilitydto) throws ParseException {
	return loanEligibilityService.checkLoanEligibility(loanEligibilitydto);
}
}
