package com.hcl.oxbank.controller;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.hcl.oxbank.dto.LoanAcceptanceDto;
import com.hcl.oxbank.dto.LoanAcceptanceResponse;
import com.hcl.oxbank.entity.Loan;
import com.hcl.oxbank.service.EMIService;
import com.hcl.oxbank.service.LoanAcceptanceService;

@RestController
public class LoanAcceptanceController 
{
    @Autowired
    public LoanAcceptanceService loanAcceptanceService;
    @Autowired
    public EMIService emiService;

    @PostMapping("/loanacceptance")
    public List<LoanAcceptanceResponse> acceptLoan(@RequestBody LoanAcceptanceDto loanAcceptancedto) 
    {
    	List<LoanAcceptanceResponse> loanAcceptance=new ArrayList<>();
        loanAcceptanceService.updateLoanStatus(loanAcceptancedto.getId());
        List<Loan> loanDetailsList = loanAcceptanceService.getLoanDetailsByCustomerId(loanAcceptancedto.getId());
        for (Loan loan : loanDetailsList) 
        {
        	LoanAcceptanceResponse response=new LoanAcceptanceResponse();
            response.setLoanAmount(loan.getLoanAmount());
            response.setTenure(loan.getTenure());
            response.setEmiAmount(emiService.calculateEMIAmount(loan.getLoanAmount(), loan.getTenure())); 
            loanAcceptance.add(response);
        }
        return  loanAcceptance;
    }  
}