package com.hcl.oxbank.controller;

import java.text.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.hcl.oxbank.dto.EligibilityResponse;
import com.hcl.oxbank.service.EligibilityService;

@RestController
public class EligibilityController 
{
	@Autowired
    EligibilityService eligibilityService;   
	
	@GetMapping("/eligibility")
	public EligibilityResponse checkEligibility(@RequestParam int workExperience, @RequestParam int ID) throws ParseException
	{
		EligibilityResponse eligibilityResponse=new  EligibilityResponse();
		boolean isEligible = eligibilityService.isCustomerEligible(workExperience, ID);
		if(isEligible==true)
		{
			eligibilityResponse.setMessage("Congratulations! You are eligible for the loan."); 
			return eligibilityResponse;
		}
		else
		{
			return eligibilityResponse;
		}
	}

    }
	


