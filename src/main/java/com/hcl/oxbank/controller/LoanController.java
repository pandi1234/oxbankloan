package com.hcl.oxbank.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.hcl.oxbank.dto.LoanDto;
import com.hcl.oxbank.service.LoanService;

@RestController
public class LoanController {

	@Autowired
	LoanService loanService;
	
	@GetMapping("/FetchingLoanDetails")
	public List<LoanDto> getLoanDetailsByCustomerId(@RequestParam int customer_Id)
	{
		return loanService.fetchLoanDetails(customer_Id);
	}
	
}
