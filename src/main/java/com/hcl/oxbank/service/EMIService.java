package com.hcl.oxbank.service;

import org.springframework.stereotype.Service;

@Service
public class EMIService {
	
	public double calculateEMIAmount(double loanAmount, int tenure)
	{
		double interestRate = 0.12;
		double monthlyInterestRate = interestRate / 12;
        double emiAmount = (loanAmount * monthlyInterestRate * Math.pow(1 + monthlyInterestRate, tenure)) / (Math.pow(1 + monthlyInterestRate, tenure) - 1);
		return emiAmount;
	}
}
