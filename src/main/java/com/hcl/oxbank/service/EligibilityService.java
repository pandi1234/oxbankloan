package com.hcl.oxbank.service; 

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.hcl.oxbank.entity.Customer;
import com.hcl.oxbank.exception.EligibilityValidationException;
import com.hcl.oxbank.repository.CustomerRepository;

@Service
public class EligibilityService {
	

	@Autowired
    CustomerRepository customerRepository;
	
    public Customer checkEligibility(int workExperience,int ID) throws EligibilityValidationException, ParseException
    {
        if(workExperience < 24)
        {
            throw new EligibilityValidationException("Sorry, You are not eligible for the loan as your Work experience is less!!");
        }
        Customer customer=customerRepository.getById(ID);
        String dob = customer.getDob();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        Date dobFormatter=formatter.parse(dob);
        Date currentDate = new Date();
        int age = calculateAge(dobFormatter, currentDate);
        if(age<24 ||age>50)
        {
            throw new EligibilityValidationException("Sorry, You are not eligible for the loan as your Age criteria is not matching");
        }
        return null;
    }
    
    public int calculateAge(Date dob, Date currentDate)
    {
        Calendar dobCalendar=Calendar.getInstance();
        dobCalendar.setTime(dob);
        Calendar currentCalendar=Calendar.getInstance();
        currentCalendar.setTime(currentDate);
        int yearsDiff = currentCalendar.get(Calendar.YEAR)-dobCalendar.get(Calendar.YEAR);
        int monthsDiff = currentCalendar.get(Calendar.MONTH)-dobCalendar.get(Calendar.MONTH);
        if(monthsDiff<0 || (monthsDiff ==0 && currentCalendar.get(Calendar.DAY_OF_MONTH)< dobCalendar.get(Calendar.DAY_OF_MONTH) ))
        {
            yearsDiff--;
        }
        return yearsDiff;
    }
    public boolean isCustomerEligible(int workExperience, int ID) throws ParseException,EligibilityValidationException {
        try {
        checkEligibility(workExperience,ID);
        return true; 
        }
        catch(EligibilityValidationException exception)
        {
            String message=exception.getMessage();
            throw new EligibilityValidationException(message);
        }
        }
}

