package com.hcl.oxbank.service;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.hcl.oxbank.Utils.ApplicationConstants;
import com.hcl.oxbank.dto.LoanDto;
import com.hcl.oxbank.entity.Customer;
import com.hcl.oxbank.entity.Loan;
import com.hcl.oxbank.exception.CustomerNotFoundException;
import com.hcl.oxbank.exception.LoanDetailsNotFoundException;
import com.hcl.oxbank.repository.CustomerRepository;
import com.hcl.oxbank.repository.LoanRepository;

@Service
public class LoanService {

	@Autowired
	LoanRepository loanRepository;
	
	@Autowired
	CustomerRepository customerRepository;
	
	@Autowired
	EMIService emiService;
	
	public Loan saveLoanDetails(Loan loan)
	{
		return loanRepository.save(loan);
	}
	public List<LoanDto> fetchLoanDetails(int customerId) throws CustomerNotFoundException
	{
		Customer customer =  customerRepository.findById(customerId)
				.orElseThrow(() ->  new CustomerNotFoundException(ApplicationConstants.customernotfound));
		List<Loan> loan =customer.getLoans();
		if(loan.isEmpty())
		{
			throw new LoanDetailsNotFoundException(ApplicationConstants.loandetailsnotfound);
		}
		else
		{
			return mapToLoanDtos(loan);
		}
		
    }  
	private List<LoanDto> mapToLoanDtos(List<Loan> loans)
	{
		List<LoanDto> loanDtos = new ArrayList<>();
		for(Loan loan:loans)
		{
			LoanDto loanDto = new LoanDto();
			loanDto.setCustomer_Id(loan.getCustomers().getId());
			loanDto.setLoanAmount(loan.getLoanAmount());
			BeanUtils.copyProperties(loan,loanDto);
			loanDto.setEMI(emiService.calculateEMIAmount(loan.getLoanAmount(), loan.getTenure()));
			loanDtos.add(loanDto);
		}
		return loanDtos;
	}
}
