package com.hcl.oxbank.service;

import java.util.List; 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.oxbank.Utils.ApplicationConstants;
import com.hcl.oxbank.entity.Customer;
import com.hcl.oxbank.entity.Loan;
import com.hcl.oxbank.exception.LoanDetailsNotFoundException;
import com.hcl.oxbank.repository.LoanRepository;

@Service
public class LoanAcceptanceService {

@Autowired
private CustomerService customerService;

@Autowired
private LoanRepository loanRepository;

  public List<Loan> getLoanDetailsByCustomerId(int customerId) throws LoanDetailsNotFoundException
  {
     Customer customer = customerService.getDataByID(customerId);
     List<Loan> loan =customer.getLoans();
     if(loan.isEmpty())
     {
         throw new LoanDetailsNotFoundException(ApplicationConstants.loandetailsnotfound);
     }
     else 
     { 
     return customer.getLoans();
     }
  }
  public void updateLoanStatus(int customerId) 
  {
     List<Loan> loanList = getLoanDetailsByCustomerId(customerId);
     Customer customer = customerService.getDataByID(customerId);
     for (Loan loan : loanList) 
     {
     loan.setLoanStatus(ApplicationConstants.loanstatusprocessing);
     }
     loanRepository.saveAll(loanList);
     customer.setLoans(loanList);
   }
}
