package com.hcl.oxbank.service;

import java.text.ParseException; 
import java.util.List; 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import com.hcl.oxbank.Utils.ApplicationConstants;
import com.hcl.oxbank.dto.LoanEligibilityDto;
import com.hcl.oxbank.dto.LoanEligibilityResponse;
import com.hcl.oxbank.entity.Customer;
import com.hcl.oxbank.entity.Loan;
import com.hcl.oxbank.exception.EligibilityValidationException;
import com.hcl.oxbank.repository.CustomerRepository;
import com.hcl.oxbank.repository.LoanRepository;

@Service
public class LoanEligibilityService {

	@Autowired
	EligibilityService eligibilityService;
	@Autowired
	EMIService emiService;
	@Autowired
	CustomerRepository customerRepository;
	@Autowired
	LoanRepository loanRepository;

	public List<Loan> getLoanDetailsByCustomerId(int ID) 
	{
		return customerRepository.findById(ID).get().getLoans();
	}

	LoanEligibilityResponse response=new LoanEligibilityResponse();

	public LoanEligibilityResponse checkLoanEligibility(@RequestBody LoanEligibilityDto loanEligibilitydto) throws ParseException,EligibilityValidationException{ 
		int workExperience =loanEligibilitydto.getWorkExperience();
		List<Loan> loanList = getLoanDetailsByCustomerId(loanEligibilitydto.getId());
		int ID =loanEligibilitydto.getId();
		for (Loan loan : loanList) {
			loan.setLoanStatus(ApplicationConstants.loanstatusrejected);
			loanRepository.saveAll(loanList);	
		}
		Customer isEligible=eligibilityService.checkEligibility(workExperience, ID);
		if (isEligible==null) {
			for (Loan loan : loanList) {
				double emi=emiService.calculateEMIAmount(loan.getLoanAmount(),loan.getTenure());
				loan.setLoanStatus(ApplicationConstants.loanstatusenquiry);
				loanRepository.saveAll(loanList);
				response.setEmi(emi);
				return response;
			} 
		}	
		return response;
}
}
