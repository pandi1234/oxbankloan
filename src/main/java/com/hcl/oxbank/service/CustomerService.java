package com.hcl.oxbank.service;

import com.hcl.oxbank.dto.CustomerDto;
import com.hcl.oxbank.Utils.ApplicationConstants;
import com.hcl.oxbank.entity.Customer;
import com.hcl.oxbank.exception.CustomerNotFoundException;
import com.hcl.oxbank.repository.CustomerRepository;
import org.springframework.beans.BeanUtils;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerService {
    @Autowired
    private CustomerRepository customerRepository;

    public Customer datasave(CustomerDto customerDto) {
        Customer customer = new Customer();
        BeanUtils.copyProperties(customerDto, customer);
        return customerRepository.save(customer);
    }

    public Customer getDataByID(int id) throws CustomerNotFoundException {
        Optional<Customer> customerId = customerRepository.findById(id);
        if (customerId.isPresent()) {
            return customerId.get();
        } else {
            throw new CustomerNotFoundException(ApplicationConstants.customernotfound);
        }
    }
}