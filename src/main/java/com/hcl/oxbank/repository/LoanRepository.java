package com.hcl.oxbank.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.hcl.oxbank.entity.Loan;

@Repository
public interface LoanRepository extends JpaRepository<Loan,Long>
{

}
