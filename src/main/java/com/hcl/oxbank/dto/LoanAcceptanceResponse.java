package com.hcl.oxbank.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoanAcceptanceResponse
{
	private long loanAmount;
	private int tenure;
	private Double emiAmount;
}

