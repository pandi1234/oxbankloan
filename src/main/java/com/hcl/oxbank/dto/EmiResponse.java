package com.hcl.oxbank.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmiResponse 
{
	private double emi;
	private int tenure;
	private long loanAmount;
}
