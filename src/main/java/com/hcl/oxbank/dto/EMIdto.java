package com.hcl.oxbank.dto;

import lombok.AllArgsConstructor; 
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor	

public class EMIdto {
	private int id ;
	private long loanAmount;
	private int tenure;
}
