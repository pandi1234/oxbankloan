package com.hcl.oxbank.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.hcl.oxbank.Utils.ApplicationConstants;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Range;
import org.springframework.format.annotation.DateTimeFormat;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomerDto {
    int id;

    @NotNull(message = ApplicationConstants.namenull)
    @NotEmpty(message = ApplicationConstants.nameempty)
    @Pattern(regexp = "^[a-zA-Z]{2,50}", message = ApplicationConstants.namepattern)
    String name;

    @NotNull(message = ApplicationConstants.mobilenull)
    @NotEmpty(message = ApplicationConstants.mobileempty)
    @Pattern(regexp = "^[0-9]{10}$", message = ApplicationConstants.mobilepattern)
    String mobile;

    @NotNull(message = ApplicationConstants.dobnull)
    @NotEmpty(message = ApplicationConstants.dobempty)
    @DateTimeFormat(pattern = ApplicationConstants.pattern)
    @JsonFormat(pattern = ApplicationConstants.jsonfrmt)
    @Pattern(regexp = "^(3[01]|[12][0-9]|0[1-9])/(1[0-2]|0[1-9])/[0-9]{4}$", message = ApplicationConstants.datepattern)
    String dob;

    @NotNull(message = ApplicationConstants.gendernull)
    @NotEmpty(message = ApplicationConstants.genderempty)
    @Pattern(regexp = "^(male|female|others)$", message = ApplicationConstants.Genderpatter)
    String gender;

    @NotNull(message = ApplicationConstants.Maritalnull)
    @NotEmpty(message = ApplicationConstants.maritalempty)
    @Pattern(regexp = "^(married|single|widow)$", message = ApplicationConstants.maritalstatuspatter)
    String maritalstatus;

    @Range(min = 0, max = 999, message = ApplicationConstants.Creditpattern)
    int creditscore;

    @Digits(fraction = 0, integer = 6, message = ApplicationConstants.salarypatter)
    int salary;

    @Digits(fraction = 0, integer = 6, message = ApplicationConstants.expensepatter)
    int expense;
}