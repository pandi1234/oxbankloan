package com.hcl.oxbank.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoanDto 
{
	private int customer_Id;
	private long loanAccountNumber;
	private double loanAmount;
	private int tenure;
	private double  interest=0.12;
	private double EMI;
	
}
