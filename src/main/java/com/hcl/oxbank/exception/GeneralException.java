package com.hcl.oxbank.exception;

import org.springframework.http.HttpStatus;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.web.context.request.WebRequest;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestControllerAdvice
public class GeneralException {
    public List<String> errorMessage = new ArrayList<>();

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    private ErrorResponse handleValidationException(MethodArgumentNotValidException e, WebRequest requeste) {
        for (ObjectError error : e.getBindingResult().getAllErrors()) {
            errorMessage.add(error.getDefaultMessage());
        }
        ErrorResponse ErrorResponse = new ErrorResponse(
                HttpStatus.BAD_REQUEST.value(),
                new Date(),
                errorMessage,
                requeste.getDescription(false)
        );
        return ErrorResponse;
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(DateException.class)
    private ErrorResponse handleFutureDateException(DateException e, WebRequest requeste) {
        errorMessage.add(e.getMessage());
        ErrorResponse ErrorResponse = new ErrorResponse(
                HttpStatus.BAD_REQUEST.value(),
                new Date(),
                errorMessage,
                requeste.getDescription(false)
        );
        return ErrorResponse;
    }

    @ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
    @ExceptionHandler(EligibilityValidationException.class)
    private ErrorResponse handleValidationException(EligibilityValidationException EligibilityValidation, WebRequest requeste) {
    	log.info(EligibilityValidation.getMessage());
    	errorMessage.add(EligibilityValidation.getMessage());
        ErrorResponse ErrorResponse = new ErrorResponse(
                HttpStatus.NOT_ACCEPTABLE.value(),
                new Date(),
                errorMessage,
                requeste.getDescription(false)
        );
        return ErrorResponse;
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(CustomerNotFoundException.class)
    private ErrorResponse handleCustomerNotFoundException(CustomerNotFoundException exception, WebRequest requeste) {
       errorMessage.add(exception.getMessage());
        ErrorResponse ErrorResponse = new ErrorResponse(
                HttpStatus.NOT_FOUND.value(),
                new Date(),
                errorMessage,
                requeste.getDescription(false)
        );
        return ErrorResponse;
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(LoanDetailsNotFoundException.class)
    private ErrorResponse handleLoanNotFoundException(LoanDetailsNotFoundException exception, WebRequest requeste) {
       errorMessage.add(exception.getMessage());
        ErrorResponse ErrorResponse = new ErrorResponse(
                HttpStatus.NOT_FOUND.value(),
                new Date(),
                errorMessage,
                requeste.getDescription(false)
        );
        return ErrorResponse;
    }
}