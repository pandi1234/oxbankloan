package com.hcl.oxbank.exception;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

@RestControllerAdvice
public class ControllerErrorHandler 
{
	 public List<String> errorMessage = new ArrayList<>();
	@ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
    @ExceptionHandler(EligibilityValidationException.class)
    private ErrorResponse handleValidationException(EligibilityValidationException EligibilityValidation, WebRequest requeste) {
            errorMessage.add(EligibilityValidation.getMessage());
        ErrorResponse ErrorResponse = new ErrorResponse(
                HttpStatus.NOT_ACCEPTABLE.value(),
                new Date(),
                errorMessage,
                requeste.getDescription(false)
        );
        return ErrorResponse;
    }
	@ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(CustomerNotFoundException.class)
    private ErrorResponse handleCustomerNotFoundException(CustomerNotFoundException notFoundException, WebRequest requeste) {
            errorMessage.add(notFoundException.getMessage());
        ErrorResponse ErrorResponse = new ErrorResponse(
                HttpStatus.NOT_FOUND.value(),
                new Date(),
                errorMessage,
                requeste.getDescription(false)
        );
        return ErrorResponse;

    }
}
