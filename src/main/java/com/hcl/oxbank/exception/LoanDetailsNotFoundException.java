package com.hcl.oxbank.exception;

public class LoanDetailsNotFoundException extends RuntimeException{

	public LoanDetailsNotFoundException(String message)
	{
		super(message);
	}
}
