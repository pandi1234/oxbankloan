package com.hcl.oxbank.exception;

public class CustomerNotFoundException extends RuntimeException{
	public CustomerNotFoundException(String message)
	{
		super(message);
	}
}