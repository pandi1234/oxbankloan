package com.hcl.oxbank.exception;

public class EligibilityValidationException extends RuntimeException
{
	public EligibilityValidationException(String message)
	{
		super(message);
	}
}
