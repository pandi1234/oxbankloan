package com.hcl.oxbank.entity;

import javax.persistence.Column;    
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "Loandetails")
public class Loan {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="Loanaccountnumber")
	private long loanAccountNumber;
	@Column(name="Loanstatus")
	private String loanStatus;
	@Column(name="Loanamount")
	private long loanAmount;
	@Column(name="Tenure")
	private int tenure;
	@ManyToOne(optional=false)
	@JoinColumn(name = "customer_ID")
	private Customer customers;
}
